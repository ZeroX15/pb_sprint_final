import { sleep } from 'k6';
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

const data = new SharedArray('Movies', function () {
  const jsonData = JSON.parse(open('../../data/static/movie.json'));

  return jsonData.movies;
});

export default () => {
  let movieIndex = __ITER % data.length;
  let movie = data[movieIndex];
  const movieID = movie._id
  const urlRes = baseRest.get(ENDPOINTS.MOVIES_ENDPOINT + `/${movieID}`, movie);
  console.log(urlRes.body)
  baseChecks.checkStatusCode(urlRes, 200)
  baseChecks.checkResponseTime(urlRes, 300)
  sleep(1);
  
}
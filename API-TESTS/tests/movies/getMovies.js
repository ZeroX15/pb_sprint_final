import { sleep } from 'k6';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

export default () => {
    const urlRes = baseRest.get(ENDPOINTS.MOVIES_ENDPOINT);
    console.log(urlRes.body)
    baseChecks.checkStatusCode(urlRes, 200)
    baseChecks.checkResponseTime(urlRes, 300)
    sleep(1);
    
}

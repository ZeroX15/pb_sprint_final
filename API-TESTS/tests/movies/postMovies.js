import { sleep } from 'k6';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

const payload = {
    "title": "A outra banana 2",
    "description": "Uma banana que se formou em artes e deu um mortal de costas",
    "launchdate": "2023-10-11",
    "showtimes": "2023-10-10"
}

export default () => {
    const urlRes = baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, payload);
    console.log(urlRes.body)
    baseChecks.checkStatusCode(urlRes, 201)
    baseChecks.checkResponseTime(urlRes, 200)
    sleep(1);
    
}
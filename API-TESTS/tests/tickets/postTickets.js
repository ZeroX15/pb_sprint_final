import { sleep } from 'k6';
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

const payload = {
    "movieId": "A banana 4",
    "userId": "Uma banana 2 que se formou em artes",
    "seatNumber": 20,
    "price": 15,
    "showtime": "2023-10-10",
}

export default () => {
    const urlRes = baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, payload);
    console.log(urlRes.body)
    baseChecks.checkStatusCode(urlRes, 201)
    baseChecks.checkResponseTime(urlRes, 300)
    sleep(1);
    
}

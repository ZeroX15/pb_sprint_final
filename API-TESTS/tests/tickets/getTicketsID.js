import { sleep } from 'k6';
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../../support/base/baseTest.js'

export const options = testConfig.options.loadThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

const data = new SharedArray('Tickets', function () {
  const jsonData = JSON.parse(open('../../data/static/ticket.json'));

  return jsonData.tickets;
});

export default () => {
  let ticketIndex = __ITER % data.length;
  let ticket = data[ticketIndex];
  const ticketID = ticket._id
  const urlRes = baseRest.get(ENDPOINTS.TICKETS_ENDPOINT + `/${ticketID}`, ticket);
  console.log(urlRes.body)
  baseChecks.checkStatusCode(urlRes, 200)
  baseChecks.checkResponseTime(urlRes, 300)
  sleep(1);
  
}
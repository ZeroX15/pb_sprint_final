import { sleep } from 'k6';
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from '../support/base/baseTest.js'

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.enviroment.hml.url;
const baseRest = new BaseRest(base_uri)
const baseChecks = new BaseChecks()

const data = new SharedArray('Movies', function () {
  const jsonData = JSON.parse(open('../data/static/movie.json'));

  return jsonData.movies;
});

const payload = {
    "title": "A outra banana 2",
    "description": "Uma banana que se formou em artes e deu um mortal de costas",
    "launchdate": "2023-10-11",
    "showtimes": "2023-10-10"
}


export default () => {
    let movieIndex = __ITER % data.length;
    let movie = data[movieIndex];
    const movieID = movie._id
    const urlRes = baseRest.put(ENDPOINTS.MOVIES_ENDPOINT + `/${movieID}`, movie);
    console.log(urlRes.body)
    baseChecks.checkStatusCode(urlRes, 200)
    baseChecks.checkResponseTime(urlRes, 300)
    sleep(1);
    
}

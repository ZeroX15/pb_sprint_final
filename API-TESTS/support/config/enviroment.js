export const testConfig = {
    enviroment: {
        hml: {
            url: 'http://localhost:3000'
        }
    },
    options: {
        smokeThresholds: {
            vus: 5,
            duration: '2s',
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            }
        },
        vinteUsuarios1Minuto: {
            vus: 20,
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            },
            stages: [
                {duration: '30s', target: 10},
                {duration: '30s', target: 10}
            ]
        },
        loadThresholds: {
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            },
            stages: [
                {duration: '10s', target: 0},
                {duration: '20s', target: 50},
                {duration: '20s', target: 50},
                {duration: '10s', target: 0}
            ]
        },
        stressThresholds: {
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            },
            stages: [
                {duration: '30s', target: 200},
                {duration: '1m', target: 200},
                {duration: '10s', target: 0}
            ]
        },
        soakThresholds: {
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            },
            stages: [
                {duration: '1m', target: 100},
                {duration: '5m', target: 100},
                {duration: '1m', target: 0}
            ]
        },
        spikeThresholds: {
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01']
            },
            stages: [
                {duration: '2m', target: 1000},
                {duration: '30s', target: 0}
            ]
        },
        breakpointThresholds: {
            thresholds: {
                http_req_duration: ['p(95)<2000'],
                http_req_failed: ['rate<0.01'],
                abortOnFail: true
            },
            stages: [
                {duration: '10m', target: 2000}
            ]
        },
    }
}
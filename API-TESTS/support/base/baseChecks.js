import { check } from 'k6';

export class BaseChecks {
    checkStatusCode(response, expectedStatus = 200){
        check( response, {
            'status code check': (r) => r.status === expectedStatus,
        })
    }

    checkResponseSize(response, expectedSize = 300){
        check( response, {
            'response size check': (r) => r.body.length <= expectedSize
        })
    }
    
    checkResponseTime(response, expectedTime = 300){
        check( response, {
            'response time check': (r) => r.timings.duration <= expectedTime
        })
    }

    checkResponseisNotEmpty(response, expectedNotEmpty = 1){
        check( response, {
            'response is not empty check': (r) => r.body.length >= expectedNotEmpty
        })
    }

}
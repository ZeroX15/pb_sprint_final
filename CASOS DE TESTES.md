# Casos de testes da Cinema API

## Instalando o Git, Node e NPM e o repositório:
Antes de começar os testes, primeiramente será necessário ter uma forma de iniciar o servidor da API, mas como ele não tem um servidor próprio, teremos que rodar localmente ou em algum lugar que hospede.
Para rodar localmente, primeiro você precisa ter o Git, o NodeJS e o NPM instalados na sua máquina local:
- <a href="https://nodejs.org/en/download">Download do NodeJS</a>
- <a href="https://docs.npmjs.com/downloading-and-installing-node-js-and-npm">Instale o Npm</a>
- <a href="https://git-scm.com/downloads">Download do Git</a>

Após ter tudo instalado, você precisa criar uma pasta em algum lugar, então clique com botão direito na pasta selecionada -> Mais opções -> Git Bash here

*Esses passos são para o Windows, mas em outros sistemas operacionais, pode ser que seja diferente.*

Isso vai abrir o terminal do Git já no caminho que leva pra pasta, agora digite esses comandos em sequência:

<code>git init</code>

<code>git clone https://github.com/juniorschmitz/nestjs-cinema.git</code>

Vai permitir ter acesso local do repositório da API, agora você vai precisar instalar as dependências do repositório, para isso, eu recomendo que use uma IDE de seu gosto.

Ao abrir a pasta dentro da IDE, você abre o terminal integrado, se você ainda está na pasta que foi criada, use esse comando no terminal:

<code>cd nestjs-cinema</code>

E então para instalar as dependências dentro da outra pasta:

<code>npm install</code>

Após a instalação, você executa o comando:

<code>npm run start</code>

Pronto, agora ele está rodando localmente na sua máquina

## Casos de teste funcionais
### Rota de filmes

- **CTF-01:** POST de filmes retorna 201 com uma mensagem que inclui o ID do filme
- **CTF-02:** GET de filmes retorna a lista completa de filmes com detalhes
- **CTF-03:** GET/ID de filmes retorna um filme existente com seus detalhes
- **CTF-04:** GET/ID de filmes retorna um erro para um filme inexistente
- **CTF-05:** PUT de filmes retorna 200 com os detalhes atualizados do filme
- **CTF-06:** DELETE de filmes retorna 204 No Content após a exclusão do filme

### Rota de ingressos

- **CTF-07:** POST de ingressos é validado somente se n° do assento está entre 0 e 99
- **CTF-08:** POST de ingressos é validado somente se preço do ingresso está entre 0 e 60
- **CTF-09:** POST de ingressos retorna 201 com uma mensagem que inclui o ID da reserva de ingresso

## Casos de teste não funcionais de performance:
### Rota de filmes

- **CTP-01:** API deve processar pelo menos 100 solicitações/s POST de filmes 
- **CTP-02:** Tempo médio de requisição deve ser no máximo 200ms de POST de filmes
- **CTP-03:** Listagem de filmes deve ocorrer em menos de 100ms
- **CTP-04:** Lista de filmes deve ser paginada, 20 filmes no máximo por página
- **CTP-05:** GET/ID de filmes deve ocorrer em menos de 50ms
- **CTP-06:** PUT de filmes deve ser processados pelo menos 50 solicitações/s
- **CTP-07:** PUT de filmes deve ocorrer em 300ms ou menos
- **CTP-08:** A API deve processar 30 solicitações DELETE de filmes por segundo
- **CTP-09:** O tempo médio de resposta de um DELETE de filme não deve passar dos 400ms

### Rota de ingressos

- **CTP-10:** A API deve processar 50 solicitações/s POST de ingressos
- **CTP-11:** Tempo médio de resposta POST de ingresso não deve passar dos 300ms
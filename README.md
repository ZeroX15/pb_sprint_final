# Nome do projeto
Cinema API

# Resumo
Será testada os aspectos funcionais e não funcionais de uma API de um cinema, ela possui aspectos básicos como gerenciamento de filmes e reseva de ingressos.

# Pessoas envolvidas
- Marcelo José
- O resto da equipe de QA (se necessário)

# Funcionalidades/Métodos a serem utilizados
Na API, só há duas rotas: filmes e ingressos, e seus métodos são os mesmos para cada uma:
- GET de filmes/ingressos
- GET/ID de filmes/ingressos
- POST de filmes/ingressos
- PUT de filmes/ingressos
- DELETE de filmes/ingressos

# Local do teste
Não há necessariamente um local físico para que o teste seja realizado, será em home office.

# Recursos necessários
- Internet
- Energia
- Uma instância da EC2 AWS disponível

# Critérios utilizados nos casos de teste
Para os testes funcionais:
- Verificar se os métodos retornam com o status correto
- Verificar se a mensagem não vem vazia
- Consequentemente, identificar se cada método está funcionando devidamente, sem bugs.

Para os testes não funcionais:
- Temos como métricas a quantidade de solicitações por segundo
- O tempo médio de resposta
- O tempo máximo (timeout)

# Riscos
- Falta de energia
- Falta de internet
- A instância do EC2 pode ser encerrada caso ficar muito tempo aberta (custeio pelo uso)
- Impedimentos ou imprevistos não listados

Para os dois primeiros: apenas esperar
Para o caso do encerramento da instância: criar uma nova instância ou entrar em contato com a Compass
Para os imprevistos: agir de acordo com a situação

# Como os resultados finais serão divulgados
Serão realizados commits dependendo do progresso e, se possível, devidamente documentados no repositório

# Cronograma
De 13/11 a 24/11, exceto feriados e finais de semana:
Os dois primeiros dias foram de configuração da instância, conhecimento da API a ser usada e preparação de um mapa mental.
Hoje, no terceiro dia, estou realizando o plano de testes.
Pretendo fazer ainda hoje os casos de testes e o ciclo de testes.
Amanhã, só Deus sabe.
